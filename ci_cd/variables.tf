
variable "image" {
  default = "CentOS-7-6-x86_64"
}

variable "flavor" {
  default = "director"
}

variable "ssh_key_file" {
  default = "~/.ssh/id_rsa"
}

variable "ssh_user_name" {
  default = "centos"
}

variable "pool" {
  default = "public"
}

variable "keypair" {
  default = "deployment"
}

variable "network" {
  default = "ed2f2e21-3d9c-4da0-8a88-4eb807e1c42c"
}
