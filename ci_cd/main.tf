resource "null_resource" "provision_tenks_vms" {
  provisioner "local-exec" {
    command = "scripts/tenks.sh provision"
  }
}

resource "openstack_compute_instance_v2" "director" {
  name            = "director"
  image_name      = "RHEL-7-7-GenericCloud"
  flavor_name     = "${var.flavor}"
  key_pair        = "${var.keypair}"

  network {
    uuid = "${var.network}"
  }
  metadata = {
    group = "director"
  }
  depends_on = [null_resource.provision_tenks_vms]
}

resource "openstack_compute_instance_v2" "mirrors" {
  name            = "mirrors"
  image_name      = "CentOS-7-6-x86_64"
  flavor_name     = "mirrors"
  key_pair        = "${var.keypair}"

  network {
    uuid = "${var.network}"
  }
  metadata = {
    group = "mirrors"
  }
  depends_on = [null_resource.provision_tenks_vms]
}

resource "null_resource" "destroy_tenks_vms" {
  provisioner "local-exec" {
    when    = "destroy"
    command = "scripts/tenks.sh destroy"
  }
}
