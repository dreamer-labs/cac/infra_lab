#!/bin/bash
script_path="$(readlink -f "${BASH_SOURCE[0]}" 2>/dev/null||echo $0)";
script_dir="$(dirname ${script_path})";
tenks_env_dir="${script_dir%}/tenks_env";
tenks_path="/tenks"

provision_virtual_ironic_servers(){
    if [ -f "${tenks_env_dir}/tenks_env.yml" ]; then
      cd $tenks_path && git pull && cd $script_dir
      if [ ! -f "${tenks_env_dir}/state.yml" ]; then
        touch ${tenks_env_dir}/state.yml
        state_file_pre_hash=$(md5sum ${tenks_env_dir}/state.yml | cut -d" " -f1)
        cp -f ${tenks_env_dir}/state.yml $tenks_path
        ansible-galaxy install --role-file=${tenks_path}/requirements.yml --roles-path=${tenks_path}/ansible/roles/
        ansible-playbook  --inventory ${tenks_path}/ansible/inventory /tenks/ansible/deploy.yml  --user tenks --extra-vars=@${tenks_env_dir}/tenks_env.yml
        ret_code=$?
        state_file_post_hash=$(md5sum ${tenks_path}/state.yml | cut -d" " -f1)
        if [ "$state_file_pre_hash" != "$state_file_post_hash" ]; then
          echo "tenks state changed copying state file back to repo directory"
          cp -f ${tenks_path}/state.yml ${tenks_env_dir}/
        else
          echo "hashes are the same. not copying state file"
        fi
        if [[ $ret_code -ne 0 ]] ; then
          exit 1
        fi
      else
        echo "state file exists. skipping tenks deployment"
      fi  
    else
        echo "missing tenks_env.yml. not running tenks deployment"
    fi
}

destroy_virtual_ironic_servers(){
    if [ -f "${tenks_env_dir}/tenks_env.yml" ]; then
      cp -f ${tenks_env_dir}/state.yml ${tenks_path}/
      ansible-galaxy install --role-file=${tenks_path}/requirements.yml --roles-path=${tenks_path}/ansible/roles/
      ansible-playbook  --inventory ${tenks_path}/ansible/inventory ${tenks_path}/ansible/teardown.yml  --user tenks --extra-vars=@${tenks_env_dir}/tenks_env.yml
      rm -f ${tenks_env_dir}/state.yml
    else
      echo "missing tenks_env.yml. not running tenks deployment"
    fi
}

main(){
  case "$1" in
    provision)
      provision_virtual_ironic_servers
      ;;
    destroy)
      destroy_virtual_ironic_servers
      ;;
  esac
 
}

main $1
