This is a document describing the tenks lab configuration

# Requirements

This tenks lab requires of course tenks which is a playbook that provisions kvm guests with virtualbmc, a single seed VM running Kolla's Openstack core services including ironic, gitlab runner, and ansible.

# Abstract

For this lab we use a seed vm running kolla openstack with ironic to provision baremetal nodes using IPMI to control them. We can use these baremetal nodes to test varius deployments such as vcenter, redhat openstack platform 13, or any other deployments that require the use of baremetal servers. 

While we could have juse used Ansible to deploy a pxe server and rotate images to provision baremetal servers, this creates more engineering and management overhead. Quite frankly, Ansible is a configuration management, keyword configuration, and is not the best tool to provision baremetal servers. This is why we are using Kolla Openstack ironic to provision baremetal nodes. We can just leverage Openstack's APIs to initiate provisioning of server and tearing them down without worrying about the orchestration aspect of it. Additionally we are using Terraform to consume the Opestack API to orchestrat the server provisioning for us. All we need to do is focus on writing and testing ansible roles that actually configure the servers.

Having a few baremetal nodes laying around is great for testing deployments like RH OSP13, but what if we really want to test the OSP operator with a large deployment? Even worse, what if we do not have any baremetal nodes to spare for testing. This is where Tenks comes in. Tenks is a playbook written specifically to provision KVM guests and configure virtualbmc for each guest to mimic a baremetal server with IPMI. This allows us to register these KVM guests to ironic as if there were baremetal servers. Now we can create a whole fleet of these virtual baremetal servers to test OSP13 and other deployments on the cheap. 


No lab is complete without Git and CI/CD. We are using a Gitlab runner for our pipeline to test our roles, provision servers, run the Ansilbe roles to deploy the services, validate the services, teardown the infrastructure while keeping our code version controlled.  


Whether you need to spin up a couple of baremetal servers to test a infrastructure as code from the ground up or test a role against a couple of servers, This project is one example that allows you to focus on testing code rather than orchestrating provisioning of services.


## Usage

* Setup the hypervisor
* Setup the Seed
* Create a provisioning docker container with terraform, openstack client, ansible, and tenks installed
* setup a ci/cd to provision servers, test code, and teardown the servers

# Setup the hypervisor

### git clone infra_lab

```
git clone https://gitlab.com/dreamer-labs/maniac/infra_lab.git
```

```
cd infra_lab
```

## Network

```
cp hypervisor/veth_interfaces/*veth* /etc/sysconfig/network-scripts/
```

```
# ./ifup-veth veth200 && ./ifup-veth veth201
```

### install openvswitch

``` 
yum install -y openvswitch
```

### configure openvswitch

```
ovs-vsctl add-br br200
```

```
ovs-vsctl add-port br200 em200
```

```
ovs-vsctl add-port br200 em201
```

### configure firewalld

```
sudo firewall-cmd --direct --permanent  --add-rule ipv4 filter FORWARD 0 -i veth201 -o veth200 -j ACCEPT
```

```
sudo firewall-cmd --direct --permanent  --add-rule ipv4 filter FORWARD 0 -i veth201 -o veth200 -m state --state RELATED,ESTABLISHED -j ACCEPT
```

```
sudo firewall-cmd --direct --permanent --add-rule ipv4 nat POSTROUTING 0 -o veth200 -j MASQUERADE
```

```
sudo firewall-cmd  --zone=public --permanent --add-port=5900-5990/tcp
```

```
sudo firewall-cmd  --zone=public --permanent --add-port=6000-6990/tcp
```

```
sudo firewall-cmd --reload
```

# Configure Seed

## Create a base centos image or copy over the base centos qcow image

## define the seed guest

```
# virsh define seed/seed.xml
```

```
# virsh start seed
```


## configure network
yum install -y bridge-utils


scp seed/veth_interfaces/* root@192.168.50.3:/etc/sysconfig/network-scripts/

ssh to the seed server as root

```
ssh root@192.168.50.3
```

```
cd /etc/sysconfig/network-scripts
```

```
ifup br0 
```

```
./ifup-veth veth0 && ./ifup-veth veth1
```

## configure control-host container

```
cd control_host/
```

```
docker build -t control-host:latest .
```


##  bootstrap the seed host

```
docker run -d -ti -v /srv:/srv --name control-host control-host:latest /bin/bash
```

```
docker exec -ti control-host /bin/bash
```

```
source /kolla_env/bin/activate
```

create ssh keypair and add public key to seed host

```
ssh-keygen
```

```
ssh-copy-id root@192.168.50.3
```

```
cd /seed
```

verify connectivity to seed host

```
ansible -i multinode -m ping all
```


```
kolla-ansible -i multinode bootstrap-servers
```

```
Kolla-ansible -i multinode deploy
```

```
source /etc/kolla/admin-openrc.sh
```

### Upload the images

```
openstack image create --disk-format qcow2 --container-format bare   --public --file /srv/rhel-7-7-server.qcow2 RHEL-7-7-GenericCloud
```

```
openstack image create --public --container-format aki \
    --disk-format aki --file /etc/kolla/config/ironic/ironic-agent.kernel deploy-vmlinuz
```

```
openstack image create --public --container-format ari \
    --disk-format ari --file /etc/kolla/config/ironic/ironic-agent.initramfs deploy-initrd
```    

## Provisioning Docker Continer

```
cd provisioner
```

```
docker build -t stack_deploy:v1 .
```

## Configure Gitlab Runner 

``` 
docker run -d --name gitlab-runner --restart always   -v /srv/gitlab-runner/config:/etc/gitlab-runner   -v /var/run/docker.sock:/var/run/docker.sock   gitlab/gitlab-runner:latest
```

```
docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register
```


